import zmq

class Zemq(object):
    def __init__(self):
        self.cx = zmq.Context()
        self.sock = self.cx.socket(zmq.PUB)
        self.sock.bind('tcp://0.0.0.0:5555')

    def send(self, *args):
        self.sock.send(*args)