from flask import Flask, request, Response
import socket
import os

from db import Db
from zemq import Zemq

app = Flask(__name__)

db = Db()
zmq = Zemq()


@app.route('/sensor/upd', methods=['POST'])
def sensor_upd():
    
    db.sensor_upd(request.json.pop('uuid'), request.json.pop('type'), request.json)
    return Response(status=200)

@app.route('/app/set_room', methods=['POST'])
def app_set_sensor():
    db.room_upd(request.form['uuid'], request.form['room'])
    return Response(status=200)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
