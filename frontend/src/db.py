import sqlite3 as sql
import os

class Db(object):

    dbfile = '/data/db.sqlite'

    def __init__(self):
        if not os.path.isfile(self.dbfile):
            con = sql.connect(self.dbfile)
            con.execute('CREATE TABLE sensors (uuid TEXT, type TEXT, room TEXT)')
            con.execute('CREATE TABLE pir (uuid TEXT, activity BOOL)')
            con.close()

    def sensor_print(self, uuid):
        with sql.connect(self.dbfile) as con:
            cur = con.cursor()

            data = cur.execute('SELECT * FROM sensors WHERE `uuid`=?', [uuid])
            #con.commit()form

            return data

    def sensor_upd(self, uuid, typ, args):
        with sql.connect(self.dbfile) as con:
            cur = con.cursor()

            if cur.execute('SELECT * FROM sensors WHERE `uuid`=?', [uuid]).fetchone() == None:
                cur.execute('INSERT INTO sensors (uuid,type) VALUES (?,?)', [uuid, typ])
            else:
                cur.execute('UPDATE sensors SET type=? WHERE uuid=?', [typ, uuid])

            if typ == 'pir':
                if cur.execute('SELECT * FROM pir WHERE `uuid`=?', [uuid]).fetchone() == None:
                    cur.execute('INSERT INTO pir (uuid,activity) VALUES (?,?)', [uuid, args['activity']])
                else:
                    cur.execute('UPDATE pir SET activity=? WHERE uuid=?', [args['activity'], uuid])


            #cur.execute('SELECT * FROM sensors WHERE `uuid`=?',)
            cur.execute('')

    def room_upd(self, uuid, room):
        with sql.connect(self.dbfile) as con:
            cur = con.cursor()

            if cur.execute('SELECT * FROM sensors WHERE `uuid`=?', [uuid]).fetchone() == None:
                cur.execute('INSERT INTO sensors (uuid,room) VALUES (?,?)', [uuid,room])

            else:
                cur.execute('UPDATE sensors SET room=? WHERE uuid=?', [room,uuid])

    