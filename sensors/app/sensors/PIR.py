import serial
from time import sleep
from uuid import uuid4

def run(dev):
    while True:
        try:
            with serial.Serial(f"/dev/{dev}") as device:
                try:
                    device.write(b"ACK\n")
                    data = {
                        "UID": device.readline().strip().decode('utf-8'),
                        "type": "PIR"
                    }
                    while True:
                        data = device.readline().decode('utf-8')
                        if data.startswith("ROOM"):
                            dl = data.split("_")
                            room = dl[1]
                            state = dl[2] == "IN"
                            data[room] = state
                    device.write(b"DCK\n")
                    device.read_all()
                    break
                except Exception as e:
                    if e is serial.SerialException:
                        raise
                    device.write(b"DCK\n")
        except serial.SerialException:
            print("Failed to connect. Retrying in one sec.")
            sleep(1)