"""
Determines if a dev is a sensor and assigns the correct driver.
"""
from threading import Thread
import serial
import importlib
import os
from time import sleep

devs = os.listdir("/dev")
usb_devs = [dev for dev in devs if ("usb" in dev.lower() or "acm" in dev.lower())]

devlist = []

for dev in usb_devs:
    with serial.Serial(f"/dev/{dev}", timeout=100) as device:
        print("Got USB device.")
        device.write(b'DCK\n')
        print("Sent DCK.")
        device.read_all()
        print("Flushed serial buffer.")
        device.write(b'WAY\n')
        print("Sent WAY")
        devtype = device.readline()
        print("Read devident.")
        if devtype.split(b"_")[0].strip() == b"SENSOR":
            print(f"Found sensor of type:{devtype.split(b'_')[1].strip()}")
            devhandler = importlib.import_module(f"app.sensors.{devtype.split(b'_')[1].strip().decode('utf-8')}")
            print("Driver available.")
            thr = Thread(target=devhandler.run, args=(dev,))
            thr.start()
            devlist.append(dev)
            print("Driver running.")

print(f"Connected to {len(devlist)} devices.")

while True:
    sleep(1)
    devs = os.listdir("/dev")
    usb_devs = [dev for dev in devs if ("usb" in dev.lower() or "acm" in dev.lower()) and dev not in devlist]
    for dev in usb_devs:
        with serial.Serial(f"/dev/{dev}", timeout=100) as device:
            device.write(b'DCK\n')
            device.read_all()
            device.write(b'WAY\n')
            devtype = device.readline()
            if devtype.split(b"_")[0].strip() == b"SENSOR":
                print(f"Found sensor of type:{devtype.split(b'_')[1].strip()}")
                devhandler = importlib.import_module(f"app.sensors.{devtype.split(b'_')[1].strip().decode('utf-8')}")
                print("Driver available.")
                thr = Thread(target=devhandler.run, args=(dev,))
                thr.start()
                devlist.append(dev)
                print("Driver running.")
    print(f"Connected to {len(devlist)} devices.")